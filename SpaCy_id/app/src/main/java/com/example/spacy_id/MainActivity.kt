package com.example.spacy_id

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageView

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Init
        val ner_btn = findViewById(R.id.ner_btn) as ImageView
        val dep_btn = findViewById(R.id.dep_btn) as ImageView

        // Intent
        ner_btn.setOnClickListener(View.OnClickListener {

            startActivity(Intent(this.applicationContext, activity_ner::class.java))
        })

        dep_btn.setOnClickListener(View.OnClickListener {

            startActivity(Intent(this.applicationContext, activity_dep::class.java))
        })
    }
}
