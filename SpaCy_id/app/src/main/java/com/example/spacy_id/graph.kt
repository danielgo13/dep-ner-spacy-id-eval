package com.example.spacy_id

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.ViewGroup
import android.widget.RelativeLayout
import com.caverock.androidsvg.SVG
import com.caverock.androidsvg.SVGImageView

class graph : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_graph)

        val layout = findViewById<RelativeLayout>(R.id.relative_layout)

        // Get intent
        var resp = intent.getSerializableExtra("response")
        // SVG init
        var svg: SVG
        var svgImage: SVGImageView

        svg = SVG.getFromString(resp.toString())

        svgImage = SVGImageView(this.applicationContext)
        svgImage.setSVG(svg)
        svgImage.layoutParams = ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )

        layout.addView(svgImage)
    }
}
