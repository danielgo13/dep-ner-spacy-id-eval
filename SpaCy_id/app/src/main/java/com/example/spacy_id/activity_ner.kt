package com.example.spacy_id

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import com.android.volley.*
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import org.json.JSONException
import java.util.HashMap

class activity_ner : AppCompatActivity() {

    lateinit var input_ner: EditText
    lateinit var queue: RequestQueue

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ner)

        input_ner = findViewById(R.id.input_ner)
        val view_ner = findViewById(R.id.view_ner) as Button

        // Volley init
        queue = Volley.newRequestQueue(this)

        view_ner.setOnClickListener(View.OnClickListener {

            predict()
        })
    }


    fun predict() {

        var intent: Intent

        var putReq = object : StringRequest(Request.Method.POST,
            "https://spacy-id.herokuapp.com/tasks/ner",
            Response.Listener<String> { response ->
                try {

                    intent = Intent(this.applicationContext, graph::class.java)
                    intent.putExtra("response", response.toString())
                    startActivity(intent)

                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(volleyError: VolleyError) {

                }
            })
        {
            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params.put("text", input_ner.text.toString())
                return params
            }
        }

        queue.add(putReq)
    }
}
