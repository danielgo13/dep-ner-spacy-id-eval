package com.example.spacy_id

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.RelativeLayout
import com.android.volley.*
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.caverock.androidsvg.SVG
import com.caverock.androidsvg.SVGImageView
import com.caverock.androidsvg.SVGParseException
import org.json.JSONException
import org.json.JSONObject
import java.util.HashMap


class activity_dep : AppCompatActivity() {

    lateinit var input_dep: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dep)

        input_dep = findViewById(R.id.input_dep) as EditText
        val view_dep = findViewById(R.id.view_dep) as Button

        view_dep.setOnClickListener(View.OnClickListener {

            predict(input_dep.text.toString())
        })
    }


    fun predict(text: String) {

        /*
        var intent: Intent

        val params = HashMap<String,String>()
        params["text"] = input_dep.text.toString()
        val jsonObject = JSONObject(params)

        val request = StringRequest(Request.Method.POST,"https://spacy-id.herokuapp.com/tasks/dep",jsonObject,
            Response.Listener<String> { response ->

                intent = Intent(this.applicationContext, graph::class.java)
                intent.putExtra("response", response.toString())
                println(input_dep.text.toString())
                println(response.toString())
                startActivity(intent)

            }, Response.ErrorListener{

            })

        request.retryPolicy = DefaultRetryPolicy(
            DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,
            // 0 means no retry
            0, // DefaultRetryPolicy.DEFAULT_MAX_RETRIES = 2
            1f // DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )

        VolleySingleton.getInstance(this).addToRequestQueue(request)
        */


        var intent: Intent
        // Volley init
        val queue = Volley.newRequestQueue(this)
        val layout = findViewById<ConstraintLayout>(R.id.layout)

        var putReq = object : StringRequest(Request.Method.POST,
            "https://spacy-id.herokuapp.com/tasks/dep",
            Response.Listener<String> { response ->
                try {

                    /*
                    intent = Intent(this.applicationContext, graph::class.java)
                    intent.putExtra("response", response.toString())
                    println(input_dep.text.toString())
                    println(response.toString())
                    startActivity(intent)
                    */
                    var svg: SVG
                    var svgImage: SVGImageView

                    svg = SVG.getFromString(response.toString())

                    svgImage = SVGImageView(this.applicationContext)
                    svgImage.setSVG(svg)
                    svgImage.layoutParams = ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT
                    )

                    layout.addView(svgImage)

                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(volleyError: VolleyError) {
                    volleyError.printStackTrace()
                }
            })
        {
            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                var params = HashMap<String, String>()
                params["text"] = text
                return params
            }
        }

        queue.add(putReq)

    }
}
